#! /system/bin/sh


config="$1"

DATA_LOG_TCPDUMPLOG_PATH=/data/debugging/netlog
DATA_LOG_TCPDUMPLOG_SIZE_AND_FILE="20|5"

function logOn() {
    # set log on command
    if [[ `getprop ro.hardware` == mt* ]]; then
        echo "mtk wlan log on"
    else
        echo "qcom wlan log on"
        setprop ctl.start wifidriverlog_always_on
    fi
    # start tcpdump log
    if [[ -d ${DATA_LOG_TCPDUMPLOG_PATH} ]]; then
        rm -rf ${DATA_LOG_TCPDUMPLOG_PATH}
    fi
    mkdir -p ${DATA_LOG_TCPDUMPLOG_PATH}
    chmod -R 777 ${DATA_LOG_TCPDUMPLOG_PATH}
    setprop sys.oplus.logkit.netlog ${DATA_LOG_TCPDUMPLOG_PATH}
    setprop persist.sys.log.tcpdump ${DATA_LOG_TCPDUMPLOG_SIZE_AND_FILE}
    setprop ctl.start tcpdumplog
}

function logOff() {
    # set log off command
    if [[ `getprop ro.hardware` == mt* ]]; then
        echo "mtk wlan log off"
    else
        echo "qcom wlan log off"
        setprop ctl.stop wifidriverlog_always_on
        chmod 0770 /data/vendor/wifi
    fi
    # stop tcpdump log
    setprop ctl.stop tcpdumplog
}

case "$config" in
    "logon")
        logOn
        ;;
    "logoff")
        logOff
        ;;
    *)
        ;;
esac
